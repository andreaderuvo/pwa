export const environment = {
  production: true,
  oauth: {
    heroku: {
      grant_type: "authorization_code",
      client_secret: "",
      client_id: "",
      scope: "identity",
      endpointToken: "https://id.heroku.com/oauth/token",
      endpointAuthorize: "https://id.heroku.com/oauth/authorize"
    }
  },
  api: {
    endpoint: "http://localhost:3000/api"
  }
};
