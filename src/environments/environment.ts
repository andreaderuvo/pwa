// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  oauth: {
    heroku: {
      grant_type: "authorization_code",
      client_secret: "",
      client_id: "",
      scope: "identity",
      endpointToken: "https://id.heroku.com/oauth/token", //POST  /callback?code=12381u39817
      endpointAuthorize: "https://id.heroku.com/oauth/authorize" //GET -> code
    }
  },
  api: {
    endpoint: "http://localhost:3000/api"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
