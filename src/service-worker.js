importScripts('./ngsw-worker.js');

let access_token = "";

self.addEventListener('install', function(event) {
    event.waitUntil(self.skipWaiting()); // Activate worker immediately
});

self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim()); // Become available to all pages
});

self.addEventListener('message', (event) => {
    if (event.data.type === 'access_token') {
        access_token = event.data.access_token;
        console.log("message", access_token);
    }
});

self.addEventListener('sync', (event) => {
    if (event.tag === 'getTodos') {
        if (access_token && access_token.length > 0) {
            console.log("waitUntil")
            event.waitUntil(getTodos());
        }
    }
});

let Constants = {
    LOCALSTORAGE_KEY_OAUHT: "oauth",
    LOCALSTORAGE_KEY_CSFR_TOKEN: "csfrToken",
    LOCALSTORAGE_LOGGED_IN: "loggedIn",
    LOCALSTORAGE_SESSION_STARTED_AT: "sessionStartedAt"
}

function getTodos() {
    console.log('Authorization: Bearer ' + access_token);
    return fetch('http://localhost:3000/api/todos', {
            headers: {
                'Authorization': 'Bearer ' + access_token
            }
        }).then(response => response.json()).then(todos => {
            console.log(todos);
            const request = indexedDB.open('pwa-db', 1);
            request.onsuccess = function(event) {
                db = event.target.result;
                todosObjectStore = db.transaction("todos-store", "readwrite").objectStore("todos-store");
                todos.forEach(function(todo) {
                    let request = todosObjectStore.put(todo);
                    request.onsuccess = function(event) {
                        console.log("[ServiceWorker] Inserted todo with id: " + todo.id)
                    };
                    request.onerror = function(event) {
                        console.log(event);
                    };
                });
            };
        })
        .catch(() => Promise.reject());
}