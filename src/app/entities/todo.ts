import { Entity } from './entity';

export interface Todo extends Entity {
    description: String;
    created_At?: Date;
    expire_at: Date;
    user_id?: Number;
    operation?: String;
}
