import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';
import { LoginService } from '../services/login.service';

@Injectable()
export class AddHeaderInterceptor implements HttpInterceptor {

    constructor(private loginService: LoginService, private authService: AuthService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("INTERCEPTOR HTTP!");
        if (req.url.startsWith(environment.api.endpoint) && this.loginService.isLoggedIn()) {
            const clonedRequest = req.clone({ headers: req.headers.append('Authorization', 'Bearer ' + this.authService.getLocalAccessToken()) });
            return next.handle(clonedRequest);
        } 
        return next.handle(req);
    }
}