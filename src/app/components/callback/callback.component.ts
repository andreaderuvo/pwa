import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'src/app/common/constants';
import { AuthService } from 'src/app/services/auth.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor(private authService: AuthService, private loginService: LoginService, private router: Router, private route: ActivatedRoute) {
    console.log("CALLBACK COMPONENT")
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params.state, localStorage.getItem(Constants.LOCALSTORAGE_KEY_CSFR_TOKEN))
      if (params.state !== localStorage.getItem(Constants.LOCALSTORAGE_KEY_CSFR_TOKEN)) {
        //passing parameters to routes: https://stackoverflow.com/questions/44864303/send-data-through-routing-paths-in-angular
        this.router.navigate(["/error", { errorMsg: "csfr error" }]);
      } else {
        console.log(params['code']);
        this.authService.getToken(params['code']).subscribe(data => {
          this.loginService.login(data);
          this.router.navigate(['/todos']);
        });
      }
    });
  }
}
