import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit, OnDestroy {

  @Input() description: string | undefined;
  @Input() expireAt: string | undefined;
  @Input() createdAt: string | undefined;
  
  constructor() {
  }

  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    console.log("ngOnInit");
  }

}
