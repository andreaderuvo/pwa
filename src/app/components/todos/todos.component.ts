import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/entities/todo';
import { TodoOnlineService } from 'src/app/services/todo_online.service';
import { TodoOfflineService } from 'src/app/services/todo_offline.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: Array<Todo> = [];

  constructor(protected todosOnlineService: TodoOnlineService, protected todosOfflineService: TodoOfflineService) {
  }

  ngOnInit(): void {
    if (navigator.onLine) {
      this.todosOnlineService.read().subscribe(todos => {
         this.todos = todos
        /*todos.forEach(todo => {
          this.todosOfflineService.update(todo);
        }); */
      });
    } else {
      this.todosOfflineService.readSyncedTodos().then(todos => {
        this.todos = todos
      });
    }
  }

  refreshsArrayAfterRemove(id: number) {
    let index = this.todos.findIndex(t => t.id === id);
    this.todos.splice(index, 1);
  }

  remove(id: Number | undefined) {
    if (navigator.onLine) {
      this.todosOnlineService.delete(id).subscribe(() => {
        this.todosOfflineService.delete(id as number).then(()=>{
          this.refreshsArrayAfterRemove(id as number);
        });
      });
    } else {
      this.todosOfflineService.read(id as number).then(todo => {
        if (todo != null) {
          todo.operation = 'C';
        }
        this.todosOfflineService.update(todo as Todo).then(() => {
          this.refreshsArrayAfterRemove(id as number);
        });
      });
    }
  }
}
