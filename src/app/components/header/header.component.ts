import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  login(event: Event) {
    this.authService.login();
  }

  public logout() {
  }

}

//https://id.heroku.com/oauth/authorize?response_type=code&client_id=72709560-be81-4db3-8bfd-44602eed510c&state=YUhwSWY1em1OdVE0ZWdiMXRrdW1NY2dCWU5CLWdjUV9DUENCQnUuOTZVaENa&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Ftodos&scope=openid%20global&code_challenge=1t-kLO2vx4wOYBccIZW-lY6FBJJwhiaru_URw-y8Ffk&code_challenge_method=S256&nonce=YUhwSWY1em1OdVE0ZWdiMXRrdW1NY2dCWU5CLWdjUV9DUENCQnUuOTZVaENa