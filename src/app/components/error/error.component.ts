import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  errorMsg: string = "";

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.errorMsg = this.route.snapshot.paramMap.get("errorMsg") as string;
  }
}