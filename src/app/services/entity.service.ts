import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Entity } from '../entities/entity';

@Injectable({
  providedIn: 'root'
})
export abstract class EntityService<E extends Entity> {
  endpoint: string = "http://localhost:3000/api";

  constructor(protected httpClient: HttpClient) {
  }

  read(): Observable<Array<E>> {
    return this.httpClient.get(`${this.endpoint}/${this.entityName}`) as Observable<Array<E>>;
  }

  readById(id: Number | undefined): Observable<E> {
    return this.httpClient.get(`${this.endpoint}/${this.entityName}/${id}`) as Observable<E>;
  }

  update(id: Number | undefined, entity: Entity) {
    return this.httpClient.patch(`${this.endpoint}/${this.entityName}/${id}`, entity);
  }

  delete(id: Number | undefined) {
    return this.httpClient.delete(`${this.endpoint}/${this.entityName}/${id}`);
  }

  abstract get entityName(): string;
}
