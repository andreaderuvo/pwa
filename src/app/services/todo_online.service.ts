import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from '../entities/todo';
import { EntityService } from './entity.service';
import { TodoOfflineService } from './todo_offline.service';

@Injectable({
  providedIn: 'root'
})
export class TodoOnlineService extends EntityService<Todo> {

  constructor(protected httpClient: HttpClient, private todoOfflineService: TodoOfflineService) {
    super(httpClient);
  }

  get entityName(): string {
    return "todos";
  }

  sync() {
    console.log("TodoOnlineService.sync");
    this.todoOfflineService.queryDeletedTodos().then(todos => {
      todos.forEach(todo => {
        this.delete(todo.id).subscribe(() => {
          console.log("deleted remotely todo with id " + todo.id);
          this.todoOfflineService.delete(todo.id as number).then(()=>{
            console.log("deleted locally todo with id " + todo.id);
          });
        });
      })
    })
  }

}