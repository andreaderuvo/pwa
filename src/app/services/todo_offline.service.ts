import { Injectable } from '@angular/core';
import { openDB, DBSchema, IDBPDatabase } from 'idb';
import { Todo } from '../entities/todo';


//https://riptutorial.com/indexeddb/example/31579/indexed-db-schema
@Injectable({
    providedIn: 'root',
})
export class TodoOfflineService {
    private db!: IDBPDatabase<TodosStore>;

    readonly DB_NAME = 'pwa-db';
    readonly DB_VERSION = 1;
    readonly TODOS_STORE = 'todos-store';

    constructor() {
       /*  try {
            this.queryDeletedTodos();
        } catch (e) {
            console.error(e);
        } */
    }

    async connectToDb() {
        this.db = await openDB<TodosStore>(this.DB_NAME, this.DB_VERSION, {
            upgrade(db) {
                let objectStore = db.createObjectStore('todos-store', {
                    keyPath: "id",
                    autoIncrement: true
                });
                objectStore.createIndex("operation" as never, "operation", {
                    unique: false
                });
            }
        });
    }

    readSyncedTodos() {
        return this.connectToDb().then(() => {
            const transaction = this.db.transaction(['todos-store']);
            const objectStore = transaction.objectStore('todos-store');
            return objectStore.getAll();
        });
    }

    syncTodos(todo: Todo) {
        //  return this.db.put('todos-store', JSON.stringify(todo), todo.id.toString());
    }

    delete(id: number) {
        return this.connectToDb().then(() => {
            return this.db.delete('todos-store', id);
        });
    }

    read(id: number): Promise<Todo | undefined> {
        return this.connectToDb().then(() => {
            const transaction = this.db.transaction(['todos-store']);
            const objectStore = transaction.objectStore('todos-store');
            return objectStore.get(id);
        });
    }

    update(todo: Todo): Promise<number> {
        return this.connectToDb().then(() => {
            const transaction = this.db.transaction(['todos-store'], "readwrite");
            const objectStore = transaction.objectStore('todos-store');
            return objectStore.put(todo);
        });
    }

    add(todo: Todo): Promise<number> {
        return this.connectToDb().then(() => {
            const transaction = this.db.transaction(['todos-store'], "readwrite");
            const objectStore = transaction.objectStore('todos-store');
            return objectStore.add(todo);
        });
    }

    queryDeletedTodos() {
        return this.connectToDb().then(() => {
            const index = this.db.transaction('todos-store').store.index('operation' as never);
            let results: Array<Todo> = [];
            return index.openCursor(IDBKeyRange.only("C")).then((cursor: any) => {
                if (cursor) {
                    results.push(cursor.value);
                    console.log(cursor.value);
                    cursor.continue();
                } else {
                    // results will have the values
                }
            }).then(() => Promise.resolve(results));
        });
    }
}

interface TodosStore extends DBSchema {
    'todos-store': {
        key: number;
        value: Todo;
    };
}