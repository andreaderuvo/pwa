import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';
import { environment } from '../../environments/environment';
import { Constants } from '../common/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  generateCSFR(): string {
    const md5 = new Md5();
    return md5.appendStr(Math.random().toString()).end().toString();
  }

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  private handleError(error: HttpErrorResponse) {
    this.router.navigate(['/error']);
    return throwError(
      'Something bad happened; please try again later.');
  }

  login() {
    let csfrToken: string = this.generateCSFR();
    localStorage.setItem(Constants.LOCALSTORAGE_KEY_CSFR_TOKEN, csfrToken);
    let authorizeUrl = `${environment.oauth.heroku.endpointAuthorize}?client_id=${environment.oauth.heroku.client_id}&response_type=code&scope=identity&state=${localStorage.getItem(Constants.LOCALSTORAGE_KEY_CSFR_TOKEN)}`;
    window.open(authorizeUrl, "_self");
  }

  getToken(code: string) {
    return this.httpClient.post(environment.oauth.heroku.endpointToken, null, {
      params: {
        grant_type: environment.oauth.heroku.grant_type,
        code: code,
        client_secret: environment.oauth.heroku.client_secret
      }
    }).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  getLocalAccessToken() {
    return JSON.parse(localStorage.getItem(Constants.LOCALSTORAGE_KEY_OAUHT) as string).access_token;
  }
}
