import { Injectable } from '@angular/core';
import { Constants } from '../common/constants';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  isLoggedIn(): boolean {
    return localStorage.getItem(Constants.LOCALSTORAGE_LOGGED_IN) != null;
  }

  logout() {
    localStorage.removeItem(Constants.LOCALSTORAGE_LOGGED_IN);
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_CSFR_TOKEN);
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_OAUHT);

    if (navigator.serviceWorker.controller) {
      navigator.serviceWorker.controller.postMessage({ type: 'access_token', access_token: "" })
    }
  }

  login(oauthPacket: any) {
    localStorage.setItem(Constants.LOCALSTORAGE_KEY_OAUHT, JSON.stringify(oauthPacket));
    localStorage.setItem(Constants.LOCALSTORAGE_LOGGED_IN, "true");
    localStorage.setItem(Constants.LOCALSTORAGE_SESSION_STARTED_AT, new Date().getTime().toString());

    if (navigator.serviceWorker.controller) {
      navigator.serviceWorker.controller.postMessage({ type: 'access_token', access_token: oauthPacket.access_token })
    }
  }
}
