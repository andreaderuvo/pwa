import { HttpClient } from '@angular/common/http';
import { ApplicationRef, Component, HostListener, OnInit } from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';
import { AuthService } from './services/auth.service';
import { LoginService } from './services/login.service';
import { TodoOnlineService } from './services/todo_online.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pwa';

  private readonly PUBLIC_VAPID_KEY_OF_SERVER = 'BJkUsZs9QYXvusPKt-2SJ_LZD8Y6L3HHEJjP9rVNaiBgjOCr5aLyGMGGJelLMi4eh-2M7SyRbQPZvEok1iYtTuA';

  constructor(private appRef: ApplicationRef, private loginService: LoginService, private authService: AuthService,
    private swPush: SwPush, private swUpdate: SwUpdate, private todosOnlineService: TodoOnlineService) {
    this.updateClient();
    this.checkUpdate();
  }

  ngOnInit(): void {
    this.pushSubscription();
    this.swPush.messages.subscribe((message) => console.log(message));
    this.swPush.notificationClicks.subscribe(({ action, notification }) => {
      window.open(notification.data.url);
    });

    this.enableSyncJob();
    this.refreshAccessTokenForServiceWorker();
    if (this.loginService.isLoggedIn()) {
      this.todosOnlineService.sync();
    }

   this.registerSyncService();
  }

  updateClient() {
    if (!this.swUpdate.isEnabled) {
      console.log('Not Enabled');
      return;
    }
    this.swUpdate.available.subscribe((event) => {
      console.log(`current`, event.current, `available `, event.available);
      if (confirm('update available for the app please conform')) {
        this.swUpdate.activateUpdate().then(() => location.reload());
      }
    });

    this.swUpdate.activated.subscribe((event) => {
      console.log(`current`, event.previous, `available `, event.current);
    });
  }

  checkUpdate() {
    this.appRef.isStable.subscribe((isStable) => {
      if (isStable) {
        const timeInterval = interval(8 * 60 * 60 * 1000);
        timeInterval.subscribe(() => {
          this.swUpdate.checkForUpdate().then(() => console.log('checked'));
          console.log('update checked');
        });
      }
    });
  }

  enableSyncJob() {
    const timeInterval = interval(30000);
    timeInterval.subscribe(() => {
     /*  if (this.loginService.isLoggedIn()) { */
        this.registerSyncService();
     /*  } */
    });
  }

  registerSyncService() {
    return navigator.serviceWorker.ready
      .then((swRegistration) => swRegistration.sync.register('getTodos'))
      .catch(() => console.log("catch registerSyncService"));
  }

  pushSubscription() {
    if (!this.swPush.isEnabled) {
      console.log('Notification is not enabled');
      return;
    }

    this.swPush
      .requestSubscription({
        serverPublicKey: this.PUBLIC_VAPID_KEY_OF_SERVER,
      })
      .then((sub) => {
        // Make a post call to serve
        console.log(JSON.stringify(sub));
      })
      .catch((err) => console.log(err));
  }

  @HostListener('window:online', ['$event'])
  isOnline(event: any) {
    console.log("PWA is online!");
    this.todosOnlineService.sync();
  }

  @HostListener('window:offline', ['$event'])
  isOffline(event: any) {
    console.log("PWA is offline!");
  }

  refreshAccessTokenForServiceWorker() {
    if (this.loginService.isLoggedIn()) {
      if (navigator.serviceWorker.controller) {
        navigator.serviceWorker.controller.postMessage({ type: 'access_token', access_token: this.authService.getLocalAccessToken() })
      }
    }
  }
}