export class Constants {
    static LOCALSTORAGE_KEY_OAUHT = "oauth";
    static LOCALSTORAGE_KEY_CSFR_TOKEN = "csfrToken";
    static LOCALSTORAGE_LOGGED_IN = "loggedIn";
    static LOCALSTORAGE_SESSION_STARTED_AT = "sessionStartedAt";
}